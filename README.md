# James Pole's Websites

This repository holds Markdown/HTML documents and the Caddy configuration for
James Pole's websites.

* [James Pole's website](//james.pole.net.nz/)
* [Mars Server website](//mars.pole.net.nz/) (default website for the server)
* [Pole Family's website](//pole.net.nz/)
