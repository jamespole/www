---
author: "James Pole"
copyright: "Copyright 2018 James Pole"
title: "Cell Sites"
---

# Cell Sites

Welcome! This is a collection of photos and information about over 170 cell sites in New Zealand and around the world.

* *Australia (coming soon)*
* *Iceland (coming soon)*
* [New Zealand](nz) (170 sites)
* *Turkey (coming soon)*
* [United Kingdom](gb) (1 site)
* *United States (coming soon)*

## Can you help identify these cell sites?

Below is a list of cell sites I have been unable to identify. If you can help identify these cell sites please
[email me](mailto:james@pole.net.nz) to let me know any information you know regarding the unidentified cell site(s)
in the photos.

* New Zealand
    * Auckland
        * [Bastion Point](nz/auk/ōrākei/bastion-point)
        * [Britomart](nz/auk/waitematā/britomart)