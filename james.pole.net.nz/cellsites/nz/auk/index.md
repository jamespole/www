---
author: "James Pole"
copyright: "Copyright 2018 James Pole"
title: "Cell Sites in Auckland"
---

# Cell Sites in Auckland

Cell sites in Auckland are categoried by the Local Board areas:

## Central areas (160 sites)

* [Albert-Eden Local Board](albert-eden) (5 sites)
* [Maungakiekie-Tāmaki Local Board](maungakiekie-tāmaki) (1 site)
* [Ōrākei Local Board](ōrākei) (52 sites)
* [Puketāpapa Local Board](puketāpapa) (2 site)
* [Waiheke Local Board](waiheke) (7 sites)
* [Waitematā Local Board](waitematā) (93 sites)

## Northern areas (2 sites)

* [Devonport-Takapuna Local Board](devonport-takapuna) (1 site)
* [Kaipātiki Local Board](kaipātiki) (1 site)

## Southern Areas (5 sites)

* [Howick Local Board](howick) (3 sites)
* [Māngere-Ōtāhuhu Local Board](māngere-ōtāhuhu) (2 site)

## Western Areas (3 sites)

* [Rodney Local Board](rodney) (2 sites)
* [Waitākere Ranges Local Board](waitākere-ranges) (1 site)