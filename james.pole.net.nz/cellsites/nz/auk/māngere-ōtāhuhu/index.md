---
author: "James Pole"
copyright: "Copyright 2018 James Pole"
title: "Cell Sites in the Māngere-Ōtāhuhu Local Board area"
---

# Cell Sites in the Māngere-Ōtāhuhu Local Board area

* [Auckland Airport](akl-airport) (1 site)
* [Māngere Bridge](māngere-bridge) (1 site)