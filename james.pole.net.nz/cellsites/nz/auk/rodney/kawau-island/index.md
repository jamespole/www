---
author: "James Pole"
copyright: "Copyright 2018 James Pole"
title: "Cell Sites on Kawau Island"
---

# Cell Sites on Kawau Island

## 2degrees/Vodafone Kawau Island

![](20160319-133053.jpg)

![](20160319-133124.jpg)

![](20180317-141159.jpg)

![](20180317-141205.jpg)

![](20180317-141755.jpg)

![](20180317-141822.jpg)

![](20180317-142136.jpg)

![](20180317-142216.jpg)