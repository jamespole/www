---
author: "James Pole"
copyright: "Copyright 2018 James Pole"
title: "Cell Sites in Oneroa"
---

# Cell Sites in Oneroa

## Spark Matiatia

![](20161016-145609.jpg)

## Vodafone Blackpool

![](20161016-143511.jpg)

![](20161016-143630.jpg)

## Vodafone Oneroa

![](20161016-144224.jpg)

![](20161016-144318.jpg)

![](20161016-144502.jpg)