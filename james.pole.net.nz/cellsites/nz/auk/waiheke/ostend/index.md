---
author: "James Pole"
copyright: "Copyright 2018 James Pole"
title: "Cell Sites in Ostend"
---

# Cell Sites in Ostend

## Spark Ostend

![](20161016-141005.jpg)

![](20161016-141056.jpg)

## Vodafone Waiheke Golf Course

![](20161016-135804.jpg)