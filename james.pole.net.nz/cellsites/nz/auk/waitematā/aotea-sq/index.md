---
author: "James Pole"
copyright: "Copyright 2018 James Pole"
title: "Cell Sites in Aotea Square"
---

# Cell Sites in Aotea Square

## Telecom Mayoral Drive

![](20120128-120515.jpg)

![](20120128-120822.jpg)

![](20120128-121054.jpg)

![](20151103-165640.jpg)

## 2degrees Mayoral Drive

![](20131102-124143.jpg)

## Vodafone Mayoral Drive

![](20131102-124041.jpg)

## Vodafone Mayoral Drive Microcell

![](20150920-185807.jpg)

## Vodafone Saint James' Theatre

![](20131102-124416.jpg)