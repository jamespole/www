---
author: "James Pole"
copyright: "Copyright 2018 James Pole"
title: "Cell Sites in the Central Business District"
---

# Cell Sites in the Central Business District

## Spark Albert Street

![](20151103-163803.jpg)

## Spark Queen Street

![](20151103-205806.jpg)

## Spark Shortland Street

![](20151103-210315.jpg)

## Spark/Vodafone Sky City

![](20150920-191016.jpg)

![](20151103-164222.jpg)

![](20151103-164534.jpg)

![](20151103-164752.jpg)

## Vodafone Auckland University Microcell

![](20150925-231655.jpg)

## Vodafone Hobson Street

![](20150920-191522.jpg)