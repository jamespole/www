---
author: "James Pole"
copyright: "Copyright 2018 James Pole"
title: "Cell Sites in Downtown"
---

# Cell Sites in Downtown

## Telecom Albert & Quay Microcell

![](20041214-152551.jpg)

## 2degrees/Telecom/Vodafone Downtown

![](20041214-153326.jpg)

![](20151103-210617.jpg)

![](20160609-142457.jpg)

![](20160609-142553.jpg)

![](20160609-142818.jpg)

![](20160903-130418.jpg)

![](20160903-130643.jpg)

## Vodafone Albert Street

![](20180702-195907c.jpg)

![](20160609-142518.jpg)

![](20160609-142603.jpg)