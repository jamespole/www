---
author: "James Pole"
copyright: "Copyright 2018 James Pole"
title: "Cell Sites in Eden Terrace"
---

# Cell Sites in Eden Terrace

## Telecom Eden Terrace

![](20120120-160943.jpg)

![](20120120-161420.jpg)

## Telecom Symonds Street Bridge

![](20151204-125333.jpg)

## Telecom Newton

![](20090703-170700.jpg)

![](20120120-162839.jpg)

![](20120120-162929.jpg)

## 2degrees Spaghetti Junction

![](20090703-164415.jpg)

![](20090703-165051.jpg)

## Vodafone Grafton Gully

![](20120128-123523.jpg)

![](20120128-123633.jpg)