---
author: "James Pole"
copyright: "Copyright 2018 James Pole"
title: "Cell Sites in Grafton"
---

# Cell Sites in Grafton

## Telecom Auckland Hospital

![](20120120-153726.jpg)

![](20120120-154046.jpg)

## Telecom Grafton

![](20120120-154844.jpg)

## Telecom Khyber Pass Road Microcell

![](20120120-160048.jpg)

![](20120120-160121.jpg)

## Vodafone Auckland Hospital

![](20040612-161409.jpg)

## Vodafone Domain COW

![](20101209-082810.jpg)

![](20101209-082834.jpg)

## Vodafone Park Road Microcell

![](20040612-160746.jpg)