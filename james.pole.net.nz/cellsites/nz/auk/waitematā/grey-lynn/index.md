---
author: "James Pole"
copyright: "Copyright 2018 James Pole"
title: "Cell Sites in Grey Lynn"
---

# Cell Sites in Grey Lynn

## Telecom Richmond Road

![](20120128-141107.jpg)

## Telecom/Vodafone Old Mill Road

![](20090324-155003.jpg)

![](20090324-155215.jpg)

![](20120128-143459.jpg)

![](20120128-143546.jpg)

## Telecom/2degrees/Vodafone Grey Lynn

![](20090703-175715.jpg)

![](20090703-175756.jpg)

![](20090703-180129.jpg)

![](20090703-180323.jpg)

## Telecom Grey Lynn Relocation

![](20090703-180300.jpg)