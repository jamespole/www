---
author: "James Pole"
copyright: "Copyright 2018 James Pole"
title: "Cell Sites in the Waitematā Local Board area"
---

# Cell Sites in the Waitematā Local Board area

## Central Business District (31 sites)

* [Aotea Square](aotea-sq) (5 sites)
* [Britomart](britomart) (1 site)
* [Central Business District](cbd) (6 sites)
* [Downtown](downtown) (3 sites)
* [Newton](newton) (6 sites)
* [Viaduct Harbour](viaduct-harbour) (2 sites)
* [Victoria Park](victoria-park) (8 sites)

## Eastern Suburbs (31 sites)

* [Eden Terrace](eden-tce) (7 sites)
* [Grafton](grafton) (7 sites)
* [Newmarket](newmarket) (4 sites)
* [Newmarket Park](newmarket-park) (5 sites)
* [Parnell](parnell) (8 sites)

## Western Suburbs (31 sites)

* [Arch Hill](arch-hill) (2 sites)
* [Grey Lynn](grey-lynn) (5 sites)
* [Herne Bay](herne-bay) (3 sites)
* [Ponsonby](ponsonby) (6 sites)
* [Saint Mary's Bay](st-marys-bay) (3 sites)
* [Westhaven](westhaven) (4 sites)
* [Westmere](westmere) (3 sites)
* [Wynyard Quarter](wynyard-quarter) (5 sites)