---
author: "James Pole"
copyright: "Copyright 2018 James Pole"
title: "Cell Sites in Newmarket"
---

# Cell Sites in Newmarket

## 2degrees/Vodafone Newmarket

![](20090809-180208.jpg)

![](20090809-180251.jpg)

![](20090809-180425.jpg)

![](20090809-180704.jpg)

![](20090809-180826.jpg)

![](20090809-181035.jpg)

![](20090809-181447.jpg)

## Vodafone Carlton Gore Road Microcell

![](20040612-163107.jpg)

## Vodafone Khyber Pass

![](20150923-134217.jpg)

## Woosh Newmarket

![](20090809-180635.jpg)
