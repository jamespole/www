---
author: "James Pole"
copyright: "Copyright 2018 James Pole"
title: "Cell Sites in Newton"
---

# Cell Sites in Newton

## Telecom Wellington Street

![](20120128-145743.jpg)

![](20160729-130520.jpg)

## 2degrees/Vodafone Karangahape Road West

![](20090703-162508.jpg)

![](20090703-162836.jpg)

## Vodafone Karangahape Road East

![](20090703-181804.jpg)

## Vodafone Spaghetti Junction

![](20090703-163657.jpg)

## Vodafone Symonds Street Bridge Microcell

![](20150917-144338.jpg)

![](20150917-144407.jpg)