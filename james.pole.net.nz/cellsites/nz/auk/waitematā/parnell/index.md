---
author: "James Pole"
copyright: "Copyright 2018 James Pole"
title: "Cell Sites in Parnell"
---

# Cell Sites in Parnell

## Telecom Parnell

![](20040612-154108.jpg)

## Telecom/2degrees Ports of Auckland

![](20131116-160223.jpg)

## Vodafone Beach Road Microcell

![](20050329-140352.jpg)

## Vodafone Cathedral Place Microcell

![](20150919-145922.jpg)

## Vodafone Parnell

![](20040612-151851.jpg)

![](20040612-151957.jpg)

## Vodafone Parnell Rise Microcell

![](20050329-135246.jpg)

## Vodafone The Strand

![](20050329-135503.jpg)