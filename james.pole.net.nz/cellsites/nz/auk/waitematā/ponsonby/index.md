---
author: "James Pole"
copyright: "Copyright 2018 James Pole"
title: "Cell Sites in Ponsonby"
---

# Cell Sites in Ponsonby

## 2degrees Ponsonby Road Central

![](20090703-155851.jpg)

## 2degrees Ponsonby Road South

![](20090703-161100.jpg)

## Vodafone Argyle Street

![](20150930-122839.jpg)

![](20150930-123039.jpg)

## Vodafone Curran Street

![](20090324-172355.jpg)

![](20150930-124128.jpg)

![](20150930-124229.jpg)

## Vodafone Jervois Road Microcell

![](20090324-180217.jpg)

## Vodafone Sarsfield Street Microcell

![](20090324-171445.jpg)
