---
author: "James Pole"
copyright: "Copyright 2018 James Pole"
title: "Cell Sites in Saint Mary's Bay"
---

# Cell Sites in Saint Mary's Bay

## Telecom Saint Mary's Bay

![](20080825-154039.jpg)

![](20080825-154126.jpg)

![](20080825-154158.jpg)

![](20080825-154708.jpg)

![](20151125-165304.jpg)

## 2degrees/Vodafone/Woosh Saint Mary's Bay

![](20080825-153338.jpg)

![](20080825-153654.jpg)

![](20080825-154842.jpg)

![](20150930-134307.jpg)

![](20150930-134528.jpg)