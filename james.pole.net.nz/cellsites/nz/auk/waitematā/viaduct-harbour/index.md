---
author: "James Pole"
copyright: "Copyright 2018 James Pole"
title: "Cell Sites in Viaduct Harbour"
---

# Cell Sites in Viaduct Harbour

## Telecom/Vodafone Viaduct Harbour

![](2016-11-20-134028.jpg)

![](2016-11-20-135223.jpg)

![](20041231-080708.jpg)

![](20041231-080826.jpg)

![](20080825-145436.jpg)

## Vodafone Te Wero Bridge

![](2016-11-20-135444.jpg)

![](20120115-222728.jpg)

![](20120115-222728.jpg)

![](20120115-222922.jpg)