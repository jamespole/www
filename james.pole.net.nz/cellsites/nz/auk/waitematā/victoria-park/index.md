---
author: "James Pole"
copyright: "Copyright 2018 James Pole"
title: "Cell Sites in Victoria Park"
---

# Cell Sites in Victoria Park

## Telecom Sturdee Street Microcell

![](20080825-160902.jpg)

## Telecom Victoria Park

![](20120128-151531.jpg)

![](20120128-152155.jpg)

![](20120128-152252.jpg)

![](20120128-152322.jpg)

## Telecom/2degrees Fanshawe Street

![](20120128-161032.jpg)

## Telecom/Vodafone Beaumont & Westhaven Microcells

![](20080825-153616.jpg)

## Vodafone Beaumont Street Microcell

![](20080825-155557.jpg)

## Vodafone Fanshawe Street Microcell

![](20080825-160233.jpg)

## Vodafone Victoria Park Microcell

![](20120128-153554.jpg)