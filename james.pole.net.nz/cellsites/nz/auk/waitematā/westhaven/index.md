---
author: "James Pole"
copyright: "Copyright 2018 James Pole"
title: "Cell Sites in Westhaven"
---

# Cell Sites in Westhaven

## Telecom Harbour Bridge South

![](20090324-173548.jpg)

## 2degrees Harbour Bridge South

![](20150930-125754.jpg)

## Vodafone Harbour Bridge DAS 1

![](20150930-130944.jpg)

![](20150930-131035.jpg)

![](20150930-131057.jpg)

![](20150930-131855.jpg)

## Vodafone Harbour Bridge DAS 2

![](20090324-173945.jpg)

![](20150930-131318.jpg)