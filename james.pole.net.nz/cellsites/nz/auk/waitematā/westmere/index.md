---
author: "James Pole"
copyright: "Copyright 2018 James Pole"
title: "Cell Sites in Westmere"
---

# Cell Sites in Westmere

## Vodafone Coxs Bay

![](20090324-163331.jpg)

## Telecom/Vodafone/Woosh Westmere

![](20090324-161651.jpg)

![](20090324-162158.jpg)

## 2degrees Westmere

![](20090324-161856.jpg)