---
author: "James Pole"
copyright: "Copyright 2018 James Pole"
title: "Cell Sites in Wynyard Quarter"
---

# Cell Sites in Wynyard Quarter

## Telecom Halsey Street Microcell

![](20080825-151116.jpg)

## Telecom Wynard Quarter

![](20080825-152315.jpg)
![](20120128-155331.jpg)

## Telecom Wynard Quarter Relocation
![](20120128-155046.jpg)
![](20120128-155348.jpg)
![](20120128-155436.jpg)
![](20120128-155731.jpg)
![](20120128-155903.jpg)

## Vodafone Wynyard Quarter East

![](20080825-152017.jpg)
![](20150930-140142.jpg)

## Vodafone Wynyard Quarter West

![](20120121-150248.jpg)
![](20120121-150316.jpg)
![](20150930-135420.jpg)
![](20150930-135458.jpg)
