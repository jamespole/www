---
author: "James Pole"
copyright: "Copyright 2018 James Pole"
title: "Cell Sites in Bastion Point"
---

# Cell Sites in Bastion Point

## Telecom Bastion Point

![](20090701-162815.jpg)

![](20171119-124253.jpg)

## Unknown Tāmaki Drive

![](20031201-124749.jpg)

![](20031201-124848.jpg)

## Vodafone Bastion Point

![](20041228-122545.jpg)

![](20041228-122629.jpg)

![](20180702-195908.jpg)