---
author: "James Pole"
copyright: "Copyright 2018 James Pole"
title: "Cell Sites in Ellerslie"
---

# Cell Sites in Ellerslie

## Telecom Abbotts Way

![](20180702-195906.jpg)

## Telecom Ellerslie Racecourse

![](20050205-130150.jpg)

![](20050205-130620.jpg)

![](20160326-161312.jpg)

![](20160326-161432.jpg)

## 2degrees/Vodafone/Woosh Ellerslie Racecourse

![](20160326-161714.jpg)

![](20160326-161941.jpg)

![](20160326-162631.jpg)

![](20160326-162946.jpg)

## Vodafone Abbotts Way

![](20180702-195906a.jpg)