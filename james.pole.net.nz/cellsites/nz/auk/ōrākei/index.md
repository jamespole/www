---
author: "James Pole"
copyright: "Copyright 2018 James Pole"
title: "Cell Sites in the Ōrākei Local Board area"
---

# Cell Sites in the Ōrākei Local Board area

## Eastern Bays (29 sites)

* [Bastion Point](bastion-point) (3 sites)
* [Kohimarama](kohimarama) (5 sites)
* [Mission Bay North](mission-bay-north) (2 sites)
* [Mission Bay South](mission-bay-south) (2 sites)
* [Ōrākei North](ōrākei-north) (2 sites)
* [Ōrākei Central](ōrākei-central) (6 sites)
* [Ōrākei South](ōrākei-south) (4 sites)
* [Saint Heliers](st-heliers) (2 sites)
* [Saint Heliers Bay](st-heliers-bay) (3 sites)

## Other Suburbs (23 sites)

* [Ellerslie](ellerslie) (4 sites)
* [Glen Innes](glen-innes) (4 sites)
* [Meadowbank](meadowbank) (3 sites)
* [Remuera East](remuera-east) (3 sites)
* [Remuera West](remuera-west) (6 sites)
* [Saint Johns](st-johns) (3 sites)
