---
author: "James Pole"
copyright: "Copyright 2018 James Pole"
title: "Cell Sites in Kohimarama"
---

# Cell Sites in Kohimarama

## Telecom/2degrees Kohimarama

![](20041228-111827.jpg)

![](20041228-111936.jpg)

![](20071231-202701.jpg)

## 2degrees Kohimarama Microcell

![](20180109-143711.jpg)

![](20180109-143754.jpg)

![](20180109-143824.jpg)

## Vodafone Grampian Road Microcell

![](20040605-160643.jpg)

## Vodafone Kohimarama North

![](20180702-195907a.jpg)

![](20040603-133503.jpg)

![](20040705-110721.jpg)

![](20041228-112548.jpg)

## Vodafone Kohimarama South

![](20080902-170120.jpg)