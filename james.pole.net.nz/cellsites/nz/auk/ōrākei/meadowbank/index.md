---
author: "James Pole"
copyright: "Copyright 2018 James Pole"
title: "Cell Sites in Meadowbank"
---

# Cell Sites in Meadowbank

## Telecom Meadowbank Village

![](20040206-151609.jpg)

## Telecom/Vodafone Meadowbank Hospital

![](20040404-161953.jpg)

![](20120120-125436.jpg)

![](20120120-125706.jpg)

![](20120120-130042.jpg)

## VHF TV Ngapuhi Road

![](20050312-114209.jpg)

![](20050312-114112.jpg)