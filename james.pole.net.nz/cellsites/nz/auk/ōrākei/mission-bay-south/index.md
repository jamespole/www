---
author: "James Pole"
copyright: "Copyright 2018 James Pole"
title: "Cell Sites in Mission Bay South"
---

# Cell Sites in Mission Bay South

## Telecom Mission Bay

![](20041228-114829.jpg)

![](20100507-111340.jpg)

## 2degrees/Vodafone Mission Bay

![](20031201-122709.jpg)

![](20050828-134152.jpg)

![](20090713-174004.jpg)

![](20171119-124253a.jpg)