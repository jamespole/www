---
author: "James Pole"
copyright: "Copyright 2018 James Pole"
title: "Cell Sites in Remuera East"
---

# Cell Sites in Remuera East

## Telecom Benson Road

![](20040206-153136.jpg)

## Telecom Rawhiti Bowling Club

![](20040731-121847.jpg)

## Vodafone Minto Road

![](20040404-160005.jpg)

![](20040404-160242.jpg)