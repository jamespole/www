---
author: "James Pole"
copyright: "Copyright 2018 James Pole"
title: "Cell Sites in Remuera West"
---

# Cell Sites in Remuera West

## Telecom Remuera Mall

![](20040222-150609.jpg)

![](20171119-124251a.jpg)

## Vodafone Brighton Road

![](20131116-140852.jpg)

![](20131116-140945.jpg)

## Vodafone Remuera Mall

![](20040222-150312.jpg)

## VHF TV Portland Road

![](20050312-103553.jpg)

![](20050312-103655.jpg)

## VHF TV Hapua Street

![](20050312-104937.jpg)

## UHF TV Remuera

![](20050205-131235.jpg)

![](20050312-112627.jpg)