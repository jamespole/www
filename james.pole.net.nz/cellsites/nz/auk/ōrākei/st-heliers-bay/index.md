---
author: "James Pole"
copyright: "Copyright 2018 James Pole"
title: "Cell Sites in Saint Heliers Bay"
---

# Cell Sites in Saint Heliers Bay

## 2degrees Saint Heliers Bay Microcell

![](20180109-130234.jpg)

## Vodafone Saint Heliers Bay

![](20180702-195907b.jpg)

![](20180702-195906b.jpg)

![](20040705-121609.jpg)

![](20040705-121922.jpg)

![](20040705-122009.jpg)

![](20080902-143406.jpg)

![](20160107-172217.jpg)

## Vodafone Saint Heliers Bay Relocation

![](20180109-130021.jpg)

![](20180109-130037.jpg)