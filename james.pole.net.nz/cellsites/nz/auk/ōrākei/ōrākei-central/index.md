---
author: "James Pole"
copyright: "Copyright 2018 James Pole"
title: "Cell Sites in Ōrākei Central"
---

# Cell Sites in Ōrākei Central

## Telecom/Vodafone Kepa Road

![](20040523-151135.jpg)

![](20090701-171517.jpg)

![](20090701-171615.jpg)

## 2degrees Kepa Road

![](20090701-171253.jpg)

## Vodafone Coates Avenue

![](20040731-130137.jpg)

## Vodafone Kepa Road

![](20040523-151152.jpg)

## Vodafone Paritai Drive Microcell

![](20040731-125135.jpg)

![](20040731-125243.jpg)

## Woosh Ōrākei

![](20040523-150705.jpg)