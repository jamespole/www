---
author: "James Pole"
copyright: "Copyright 2018 James Pole"
title: "Cell Sites in Ōrākei North"
---

# Cell Sites in Ōrākei North

## Telecom/2degrees Okahu Bay

![](20080825-142820.jpg)

![](20090701-164025.jpg)

![](20090701-164435.jpg)

![](20101227-115635.jpg)

## Vodafone Okahu Bay

![](20040523-152235.jpg)

![](20050828-142848.jpg)

![](20080825-143133.jpg)

![](20080825-143319.jpg)

![](20101227-115337.jpg)

![](20101227-115519.jpg)

![](20101227-115717.jpg)