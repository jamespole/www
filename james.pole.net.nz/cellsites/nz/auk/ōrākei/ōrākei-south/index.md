---
author: "James Pole"
copyright: "Copyright 2018 James Pole"
title: "Cell Sites in Ōrākei South"
---

# Cell Sites in Ōrākei South

## Telecom Ōrākei Creek Relocation

![](20080906-105524.jpg)

## Telecom/Vodafone Ōrākei Creek

![](20040206-154526.jpg)

![](20040206-154851.jpg)

## 2degrees Ōrākei Creek

![](20080906-103927.jpg)

![](20080906-104154.jpg)

## Vodafone Ōrākei Creek Relocation

![](20080906-102615.jpg)

![](20080906-102828.jpg)

![](20080906-103059.jpg)