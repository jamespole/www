---
author: "James Pole"
copyright: "Copyright 2018 James Pole"
title: "Cell Sites in Aotearoa (New Zealand)"
---

# Cell Sites in Aotearoa (New Zealand)

* [Auckland](auk) (170 sites)
* *Canterbury (coming soon)*
* *Waikato (coming soon)*
* *Wellington (coming soon)*

## Networks in Aotearoa

* 2degrees
* Spark *(formerly Telecom)*
* Vodafone *(formerly BellSouth)*
* Woosh

## Frequencies in Aotearoa

### 2degrees

* <del>GSM 900/1800</del>
* UMTS 900/2100
* LTE 700/900/1800 (bands 28/8/3)

### Spark

* <del>AMPS 850</del>
* <del>D-AMPS 850</del>
* <del>CDMA 850</del>
* UMTS 850/2100
* LTE 700/1800 (bands 28/3)

### Vodafone

* GSM 900/<del>1800</del>
* UMTS 900/2100
* LTE 700/1800 (bands 28/3)