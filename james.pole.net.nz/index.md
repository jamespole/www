---
author: "James Pole"
copyright: "Copyright 2018 James Pole"
title: "James Pole"
---

# James Pole

Welcome to James Pole's home on the world wide web!

## My contact details

* Email: <james@pole.net.nz>
* SMS: [+64 21 322 756](tel:+6421322756)

## My stuff

* [Cell sites](cellsites)
* Checklists
    * [Packing list](packlist)
* Code
    * [GitHub](//github.com/jamespole)
    * [GitLab](//gitlab.com/jamespole)
* [Photos](//jamespole.smugmug.com/)
* [Start page](start)