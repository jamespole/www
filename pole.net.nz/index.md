---
author: "James Pole"
copyright: "Copyright 2018 James Pole"
title: "Pole Family"
---

# Pole Family

Welcome to the Pole Family's home on the world wide web!

## James Pole

* Homepage: [james.pole.net.nz](//james.pole.net.nz)
* Email: <james@pole.net.nz>
* Facetime/SMS: [+64 21 322 756](tel:+6421322756)

## Marja Pole

* Email: <marja@pole.net.nz>